import random

import requests

from cloudbot import hook

api_url = "https://g.tenor.com/v1/search?q={}&key={}&limit={}"


@hook.command("tenor", "gif")
def giphy(text, bot):
    """<query> - Searches tenor.com for a gif using the provided search term."""
    api_key = bot.config.get_api_key("tenor")
    term = text.strip()
    search_url = api_url.format(term, api_key, 1)
    results = requests.get(search_url)
    if results.status_code == 200: 
      json = results.json()
      out = "{} (Powered by Tenor)".format(json["results"][0]["itemurl"])
    else:
      out = "Not Found."

    return out
