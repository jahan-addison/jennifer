import random

import requests

from udpy import UrbanClient
from cloudbot import hook
from cloudbot.util import formatting

@hook.command("urban", "u", "ud", autohelp=False)
def urban(text, reply):
    """<phrase> [id] - Looks up <phrase> on urbandictionary.com."""

    headers = {"Referer": "http://m.urbandictionary.com"}
    
    ud = UrbanClient()

    if text:
        # clean and split the input
        text = text.lower().strip()
        parts = text.split()

        # if the last word is a number, set the ID to that number
        # but not if its the only word, in which case the number is the query
        if parts[-1].isdigit() and len(parts) > 1:
            id_num = int(parts[-1])
            # remove the ID from the input string
            del parts[-1]
            text = " ".join(parts)
        else:
            id_num = 1

        page = ud.get_definition(text)
    else:
        # get a random definition!
        page = ud.get_random_definition()
        id_num = None

    definitions = sorted(page, key=lambda df: df.upvotes, reverse=True)

    if not definitions:
        return "Not found."

    if id_num:
        # try getting the requested definition
        try:
            definition = definitions[id_num - 1]
            def_text = definition.definition
        except IndexError:
            return "Not found."

        output = "[{}/{}]: {}".format(
            id_num, len(definitions), def_text
        )

    else:
        output = "{}".format(definitions[0])

    return output
